BANCO-INTER-TEST API
=====================

This is a REST API for Banco Inter exam. 

See [the spec](https://exam.bancointer.com.br/prova.html#jobs).

## SetUp

- fork and clone this repo.
- run `mvn install` from your terminal (make sure you `cd` into the correct directory first).
- database used is the H2.
- run `mvm spring-boot: run` from your terminal to start the application.
- test the app using [Postman](https://www.getpostman.com/)

## API Endpoints:

#JOB

1. `/jobs` GET all jobs

2. `/jobs/id` GET specific job by id

3. `/jobs` POST (aka create) a new Job

Request body

{
  "id": 1,
  "name": "Job1",
  "active": true,
  "parentJob": {
    "id": 2,
    "name": "Second job",
    "active": true
  },
  "tasks": [
    {
      "id": 1,
      "name": "First task",
      "weight": 5,
      "completed": true,
      "createdAt": "2015-05-23"
    },
    {
      "id": 2,
      "name": "Second task",
      "weight": 2,
      "completed": false,
      "createdAt": "2017-05-23"
    }
  ]
}



4. `/jobs/id` PUT (update) an Job

Request body

{
  "id": 1,
  "name": "First Job",
  "active": true,
  "parentJob": {
    "id": 2,
    "name": "job 3",
    "active": true
  },
  "tasks": [
    {
      "id": 1,
      "name": "First task",
      "weight": 2,
      "completed": true,
      "createdAt": "2015-05-23"
    },
    {
      "id": 2,
      "name": "Second task",
      "weight": 4,
      "completed": false,
      "createdAt": "2017-05-23"
    }
  ]
}


5. `/jobs/id` DELETE an Job

#TASK

1. `/task` GET all jobs

2. `/task/id` GET specific job by id

3. `/task` POST (aka create) a new Task

{
  "id": 5,
  "name": "task 5",
  "weight": 2,
  "completed": false,
  "createdAt": "2018-08-23"
}

4. `/task/id` PUT (update) an Task

{
  "id": 1,
  "name": "task 5",
  "weight": 5,
  "completed": true,
  "createdAt": "2018-08-23"
}

5. `/task/id` DELETE an Task







