package br.com.squadra.exam.exception;

public class NotFoundDaoException extends RuntimeException{

    public NotFoundDaoException(String message) {
        super(message);
    }


}
