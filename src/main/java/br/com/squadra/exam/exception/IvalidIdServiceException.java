package br.com.squadra.exam.exception;

public class IvalidIdServiceException extends RuntimeException{
    public IvalidIdServiceException(String message) {
        super(message);
    }
}
