package br.com.squadra.exam.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "jobs",
    indexes ={@Index(
        columnList="name",
        unique=true,
        name="unique_jobs_name")
    }
)
public class Job implements Serializable{

    @Id
    @Column(nullable = false)
    private Integer id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private Boolean active;

    @OneToMany(mappedBy = "jobId", cascade = CascadeType.ALL)
    private List<Task> tasks;

    @OneToOne(mappedBy = "parent", cascade = CascadeType.ALL)
    private Job parentJob;

    @OneToOne
    @JoinColumn(name = "id_parent_fk")
    private Job parent;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;

    }

    public Job getParentJob() {
        return parentJob;
    }

    public void setParentJob(Job parentJob) {
        this.parentJob = parentJob;
    }

    public void setParent(Job parent) {
        this.parent = parent;
    }

    public void addTask(Task task){
        if(this.tasks == null){
            this.tasks = new ArrayList<>();
        }

        task.setJobId(this);
        this.tasks.add(task);
    }

    public void addParentJob(Job job){
        job.setParent(this);
        this.parentJob = job;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Job job = (Job) o;

        return id != null ? id.equals(job.id) : job.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Job{" +
                "id=" + id +
                '}';
    }
}
