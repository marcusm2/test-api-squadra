package br.com.squadra.exam.domain;

import org.springframework.http.HttpStatus;

import java.io.Serializable;

public class ErrorDatail implements Serializable {
    private Integer statusCode;
    private String statusMessage;
    private String httpMethod;
    private String error;
    private String detail;
    private String path;

    public Integer getStatusCode() {
        return statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    public String getError() {
        return error;
    }

    public String getDetail() {
        return detail;
    }

    public String getPath() {
        return path;
    }

    public static Builder builder(){
        return new Builder();
    }

    public static class Builder{

        private ErrorDatail error;

        Builder(){
            this.error = new ErrorDatail();
        }

        public Builder addStatus(HttpStatus status){
            this.error.statusCode = status.value();
            this.error.statusMessage = status.getReasonPhrase();
            return this;
        }

        public Builder addHttpMethod(String method){
            this.error.httpMethod = method;
            return this;
        }

        public Builder addError(String error){
            this.error.error = error;
            return this;
        }

        public Builder addDetail(String detail){
            this.error.detail = detail;
            return this;
        }

        public Builder addPath(String path){
            this.error.path = path;
            return this;
        }

        public ErrorDatail build(){
            return this.error;
        }
    }
}
