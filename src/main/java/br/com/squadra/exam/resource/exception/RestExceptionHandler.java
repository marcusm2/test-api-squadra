package br.com.squadra.exam.resource.exception;

import br.com.squadra.exam.domain.ErrorDatail;
import br.com.squadra.exam.exception.NotFoundDaoException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {


    @ExceptionHandler({org.hibernate.exception.ConstraintViolationException.class})
    public ResponseEntity<Object> violatedConstraint(org.hibernate.exception.ConstraintViolationException ex, WebRequest request){
        return handleExceptionInternal(
                ex, ErrorDatail.builder()
                        .addDetail("constraint violated: " + ex.getConstraintName())
                        .addError(ex.getMessage())
                        .addStatus(HttpStatus.CONFLICT)
                        .addHttpMethod(getHttpMethod(request))
                        .addPath(getPath(request))
                        .build(),
                new HttpHeaders(), HttpStatus.CONFLICT, request);
    }

    @ExceptionHandler({org.hibernate.PropertyValueException.class})
    public ResponseEntity<Object> entityNotFound(org.hibernate.PropertyValueException ex, WebRequest request) {

        return handleExceptionInternal(
                ex, ErrorDatail.builder()
                        .addDetail("the '" + ex.getPropertyName() + "' attribute can not be null")
                        .addError(ex.getMessage())
                        .addStatus(HttpStatus.BAD_REQUEST)
                        .addHttpMethod(getHttpMethod(request))
                        .addPath(getPath(request))
                        .build(),
                new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler({NotFoundDaoException.class})
    public ResponseEntity<Object> entityNotFound(NotFoundDaoException ex, WebRequest request){
        return handleExceptionInternal(
                ex, ErrorDatail.builder()
                        .addDetail("resource not found in database")
                        .addError(ex.getMessage())
                        .addStatus(HttpStatus.NOT_FOUND)
                        .addHttpMethod(getHttpMethod(request))
                        .addPath(getPath(request))
                        .build(),
                new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler({NullPointerException.class, IllegalArgumentException.class})
    public ResponseEntity<Object> serverException(RuntimeException ex, WebRequest request){
        return handleExceptionInternal(
                ex, ErrorDatail.builder()
                    .addDetail("an exception has been thrown")
                    .addError(ex.getMessage())
                    .addStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                    .addHttpMethod(getHttpMethod(request))
                    .addPath(getPath(request))
                    .build(),
                new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

    private String getPath(WebRequest request){
        return ((ServletWebRequest) request).getRequest().getRequestURI();
    }

    private String getHttpMethod(WebRequest request){
        return ((ServletWebRequest) request).getRequest().getMethod();
    }
}
