package br.com.squadra.exam.service;

import br.com.squadra.exam.domain.Job;
import br.com.squadra.exam.domain.Task;

import java.util.List;

public interface JobService {
    void save(Job job);
    void update(Integer id, Job job);
    void delete(Integer id);
    Job findById(Integer id);
    List<Job> findAll();

}
