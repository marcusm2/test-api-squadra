package br.com.squadra.exam.service;

import br.com.squadra.exam.domain.Task;

import java.util.List;

public interface TaskService {
    void save(Task task);
    void update(Integer id, Task task);
    void delete(Integer id);
    Task findById(Integer id);
    List<Task> findAll();
}
