package br.com.squadra.exam.service;

import br.com.squadra.exam.dao.JobDao;
import br.com.squadra.exam.domain.Job;
import br.com.squadra.exam.exception.IvalidIdServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
@Transactional
public class JobServiceImpl implements JobService{

    @Autowired
    private JobDao jobDao;

    @Override
    public void save(Job job) {
        jobDao.save(job);

        if (job.getTasks() != null){
            job.getTasks()
                    .parallelStream()
                    .forEach(job::addTask);
        }

        if (job.getParentJob() != null){
            job.addParentJob( job.getParentJob());
        }
    }

    @Override
    public void update(Integer id, Job job) {
        job.setId(validId(id));
        jobDao.update(job);
    }

    @Override
    public void delete(Integer id) {
        jobDao.delete(validId(id));
    }

    @Override
    @Transactional(readOnly = true)
    public Job findById(Integer id) {
        return jobDao.findById(validId(id));
    }

    @Override
    @Transactional(readOnly = true)
    public List<Job> findAll() {
        return jobDao.findAll();
    }

    private Integer validId(Integer id){
        if(id <= 0){
            throw new IvalidIdServiceException("the id field value is invalid!");
        }
        return id;
    }
}
