package br.com.squadra.exam.service;

import br.com.squadra.exam.dao.TaskDao;
import br.com.squadra.exam.domain.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class TaskServiceImpl implements TaskService{

    @Autowired
    private TaskDao dao;

    @Override
    public void save(Task task) {
        dao.save(task);
    }

    @Override
    public void update(Integer id, Task task) {
        task.setId(id);
        dao.update(task);
    }

    @Override
    public void delete(Integer id) {
        dao.delete(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Task findById(Integer id) {
        return dao.findById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Task> findAll() {
        return dao.findAll();
    }
}
