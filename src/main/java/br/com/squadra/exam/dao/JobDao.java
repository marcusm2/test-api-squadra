package br.com.squadra.exam.dao;

import br.com.squadra.exam.domain.Job;

import java.util.List;

public interface JobDao {

    void save(Job job);
    void update(Job job);
    void delete(Integer id);
    Job findById(Integer id);
    List<Job> findAll();
}
