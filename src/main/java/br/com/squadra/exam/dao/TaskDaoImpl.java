package br.com.squadra.exam.dao;

import br.com.squadra.exam.domain.Task;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class TaskDaoImpl implements TaskDao{

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(Task task) {
        entityManager.persist(task);
    }

    @Override
    public void update(Task task) {
        entityManager.merge(task);
    }

    @Override
    public void delete(Integer id) {
        entityManager.remove(entityManager.getReference(Task.class, id));
    }

    @Override
    public Task findById(Integer id) {
        return  entityManager.find(Task.class, id);
    }

    @Override
    public List<Task> findAll() {
        return entityManager
                .createQuery("SELECT c FROM Task c", Task.class)
                .getResultList();
    }
}
