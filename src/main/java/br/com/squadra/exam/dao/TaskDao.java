package br.com.squadra.exam.dao;

import br.com.squadra.exam.domain.Task;

import java.util.List;

public interface TaskDao {
    void save(Task task);
    void update(Task task);
    void delete(Integer id);
    Task findById(Integer id);
    List<Task> findAll();
}
