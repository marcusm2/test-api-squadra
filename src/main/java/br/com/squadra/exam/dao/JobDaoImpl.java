package br.com.squadra.exam.dao;

import br.com.squadra.exam.domain.Job;
import br.com.squadra.exam.exception.NotFoundDaoException;
import org.springframework.stereotype.Repository;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class JobDaoImpl implements JobDao{

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(Job job) {
        entityManager.persist(job);
    }

    @Override
    public void update(Job job) {
        entityManager.merge(job);
    }

    @Override
    public void delete(Integer id) {
        try {
            entityManager.remove(entityManager.getReference(Job.class, id));
        }catch (EntityNotFoundException ex){
            throw new NotFoundDaoException(("Job not found for id = " + id + "."));
        }


    }

    @Override
    public Job findById(Integer id) {
        Job job = entityManager.find(Job.class, id);

        if(job == null){
            throw new NotFoundDaoException("Job not found for id = " + id + ".");
        }
        return job;
    }

    @Override
    public List<Job> findAll() {
        return entityManager
                .createQuery("SELECT c FROM Job c", Job.class)
                .getResultList();
    }
}
